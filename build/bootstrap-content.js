const contentful = require('contentful/dist/contentful.node');
const fs = require('fs');
const path = require('path');
const shell = require('shelljs');
const _ = require('lodash');
const chalk = require('chalk');
const config = require('../config');
const contentConfig = require('../config/secure/content-config');
const marked = require('marked');

// custom interpolate string used to disable ES variable option in lodash
_.templateSettings.interpolate = /{{{([\s\S]+?)}}}/g;

const client = contentful.createClient({
    space: contentConfig.space,
    accessToken: contentConfig.accessToken
});

const CONTENT_DIR = config.build.assetsSubDirectory;
const bootstrapPath = path.join(
    config.build.assetsRoot, config.build.assetsSubDirectory)

shell.rm('-rf', CONTENT_DIR);
shell.mkdir('-p', CONTENT_DIR);

function saveToFile(contentObj, filename, extension) {
    fs.writeFileSync(
        `${CONTENT_DIR}/${filename}.${extension}`, contentObj
    );
}

function saveToJsonFile(contentObj, filename) {
    saveToFile(
        JSON.stringify(contentObj, null, 4),
        filename,
        'json'
    );
}

function moveFiles() {
    shell.config.silent = true
    shell.cp('-R', 'static/*', bootstrapPath)
    shell.config.silent = false
}

// function parseMarkdownSections(content) {
//     if (content && content.length > 0) {
//         content = content[0];
//         let sections = _.keys(content.fields).filter(function(key) {
//             return key.indexOf('section') === 0 || key.indexOf('content') === 0
//         });
//         // custom section maker
//         sections.forEach(function(sectionKey, i) {
//             let sectionContent = content.fields[sectionKey];
//             sectionContent = sectionContent.split('\n---\n---\n');
//             sectionContent = sectionContent.map(function(section, index) {
//                 section = marked(section);
//                 section = section.split('<hr>');
//                 let colSize = 12 / (section.length);
//                 section = section.map(function(subSection) {
//                     return `<div class="col-md-${colSize}">${subSection}</div>`;
//                 })
//                 let sectionContent = section.join('\n');
//                 section = `<div class="row">${sectionContent}</div>`;
//                 return section;
//             });
//             content.fields[sectionKey] = sectionContent.join('\n');
//         });
//         return content;
//     } else {
//         throw new Error('content must be an array');
//     }
// }

function renderTemplateToString(data, hbsString) {
    console.log(chalk.green('Rendering Template'))
    // console.log('template >>> ', hbsString)
    let compiled = _.template(hbsString);
    // console.log(compiled(data));
    return compiled(data);
}

function compilePage(pageData, pageName, layout) {
  console.log(chalk.green(`Compiling Page ${pageName}...`))
  // layout mapped to template field in contentful (default, contact, gallery)
  if (!layout) layout = 'default';
  fs.readFile(
    // `src/layouts/${layout}.vue`,
    // use a basic vue template to store data and template that imports a layout template
    'build/generic-template.hbs',
    'utf8',
    function(err, layoutTemplate) {
      if (err) console.log(chalk.red(err))
      let templateData = {
        pageContent: pageData.content,
        pageData: JSON.stringify(_.pick(pageData, [
            'section1',
            'section2',
            'browserTitle',
            'title',
            'slug',
            'mainImage',
            'mediaGallery',
            'slider'
        ]), null, 4),
        pageTemplate: pageData.template
      }
      let fileContents = renderTemplateToString(templateData, layoutTemplate);
      fs.writeFile(
        `${CONTENT_DIR}/${pageName}.vue`,
        fileContents,
        function(err, data) {
          if (err) {
            console.log(chalk.red(`${pageName} save failed. \n ${err}`))
          } else {
            console.log(chalk.green(`${pageName} vue file saved.`))
          }
        });
    });
}

function getPages() {
  return client.getEntries({
    content_type: 'page',
    order: 'fields.mainNavOrder',
    include: 2
  });
}

function getGalleries() {
  return client.getEntries({
    content_type: 'gallery'
  });
}

function getSiteConfig() {
  return client.getEntries({
    content_type: 'siteConfig'
  });
}

// function getMenus() {
//     return client.getEntries({content_type: 'menu', include: 1})
// }

function pushNavItem(array, page) {
    array.push({
        title: page.title,
        slug: page.slug,
        order: page.mainNavOrder,
        template: page.template
    });
}

// var exports = module.exports = function() {

    console.log(chalk.bold.blue('Getting entries...'));
    return Promise.all([
            getSiteConfig(),
            getPages(),
            getGalleries(),
            // getMenus()
        ])
        .then((data) => {
            let siteConfig = data[0].items[0].fields;
            let pages = data[1].items;
            let galleries = data[2].items;
            // let menus = data[3].items;
            siteConfig.navigation = [];
            siteConfig.mainMenu = [];
            pages.forEach(page => {
                pushNavItem(siteConfig.navigation, page.fields);
                if (page.fields.mainNavOrder) {
                    pushNavItem(siteConfig.mainMenu, page.fields);
                }
                // page = parseMarkdownSections([page]);
                // join gallery content type to media page
                if (page.fields.slug === 'media') {
                    page.fields.galleries = galleries
                }
                compilePage(page.fields, page.fields.slug, page.fields.template);
                // saveToJsonFile(page.fields, page.fields.slug)
            });
            siteConfig.mainMenu =
                _.orderBy(siteConfig.mainMenu, ['order'], ['asc']);

            // console.log(menus[0].fields.item)
            // menus.forEach(menu => {
            //   menu = menu.fields;
            //   siteConfig.mainMenu.push(
            //     _.pick(menu, ['title', 'slug', 'mainNavOrder']))
            // });
            saveToJsonFile(siteConfig, 'site-config');
            moveFiles();
        })
        .catch((err) => console.log(err));
// }

