import Vue from 'vue';
import Router from 'vue-router';
// import Home from 'components/Home';
import Home from '../../static/home';

// scrollBehavior:
// - only available in html5 history mode
// - defaults to no scroll behavior
// - return false to prevent scroll
const scrollBehavior = (to, from, savedPosition) => {
  if (savedPosition) {
    // savedPosition is only available for popstate navigations.
    return savedPosition
  } else {
    const position = {}
    // new navigation.
    // scroll to anchor by returning the selector
    if (to.hash) {
      position.selector = to.hash
    }
    // check if any matched route config has meta that requires scrolling to top
    if (to.matched.some(m => m.meta.scrollToTop)) {
      // cords will be used if no selector is provided,
      // or if the selector didn't match any element.
      position.x = 0
      position.y = 0
    }
    // if the returned position is falsy or an empty object,
    // will retain current scroll position.
    return position
  }
}

export default function router(siteConfig) {
    Vue.use(Router);
    let routes = [];
    siteConfig.navigation.forEach(nav => {
        let path = nav.slug
        if (nav.slug === 'home') path = ''
        let route = {
            path: '/' + path,
            name: nav.slug,
            component: () => import(`../../static/${nav.slug}.vue`)
        };
        routes.push(route);
    });
    // routes.push({
    //     path: '/media/:galleryId',
    //     component: () => import(`../../static/media.vue`)
    // })
    return new Router({
        linkActiveClass: 'active',
        mode: 'history',
        scrollBehavior,
        routes: routes,
    });
}
