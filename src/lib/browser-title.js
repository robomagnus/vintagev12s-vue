export default {
    created() {
        this.setBrowserTitle();
    },
    watch: {
        'page.browserTitle': 'setBrowserTitle',
        '$route': 'setBrowserTitle'
    },
    methods: {
        // set the title from a watcher
        setBrowserTitle: function() {
            if (this.page && (this.page.browserTitle || this.page.title)) {
                let title = this.page.browserTitle || this.page.title;
                this.$root.$emit('set-browser-title', title)
            } else {
                // reset the title to basic clearing previous page setting
                this.$root.$emit('set-browser-title')
            }
        }
    }
}
