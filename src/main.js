import Vue from 'vue'

import App from './App'
import router from './router'

import Popper from 'popper.js/dist/umd/popper.js'
import boostrap from 'bootstrap/dist/js/bootstrap'

// import alert from 'bootstrap/js/dist/alert'
// import button from 'bootstrap/js/dist/button'
// import carousel from 'bootstrap/js/dist/carousel'
// import collapse from 'bootstrap/js/dist/collapse'
// import dropdown from 'bootstrap/js/dist/dropdown'
// import modal from 'bootstrap/js/dist/modal'
// import popover from 'bootstrap/js/dist/popover'
// import scrollspy from 'bootstrap/js/dist/scrollspy'
// import tab from 'bootstrap/js/dist/tab'
// import tooltip from 'bootstrap/js/dist/tooltip'
// import util from 'bootstrap/js/dist/util'

import checkMobile from './lib/check-mobile'

import Scroller from './components/Scroller'
import FooterNav from './components/footer-nav'
import modalGallery from './components/Modal-Gallery'
import carousel from './components/Carousel'

import siteMeta from '!../static/site-config.json'
import browserTitle from './lib/browser-title'
import VueScrollbar from 'vue2-scrollbar'

Vue.component('scroller', Scroller)
Vue.component('footerNav', FooterNav)
Vue.component('modalGallery', modalGallery)
Vue.component('carousel', carousel)
Vue.component('vue-scrollbar', VueScrollbar)

window.store = {
    state: {
        carouselStart: 0
    },
    changeCarouselStart(index) {
        this.state.carouselStart = index
    }
}

new Vue({
    el: '#App',
    mixins: [browserTitle],
    router: router(siteMeta),
    render: (h) => h(App),
    created() {
        if (this.checkMobile) {
            $('body').addClass('mobile')
        }
    },
    data() {
        return {
            siteMeta: siteMeta,
            checkMobile: checkMobile(),
            scroll: 0,
            state: store.state
        }
    }
})
